import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../api.service';
//import {swal} from 'sweetalert';
import {User} from '../user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public registerForm: FormGroup;
  public sumbitAttempt: boolean = false;

  constructor(public formBuilder: FormBuilder, private apiService:ApiService) {
    this.registerForm = formBuilder.group({
			username:['', Validators.compose([Validators.maxLength(30), Validators.minLength(6), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      password:['', Validators.compose([Validators.maxLength(30), Validators.minLength(6), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      passwordConfirm:['', Validators.compose([Validators.maxLength(30), Validators.minLength(6), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
		});

   }

  register(){
    this.sumbitAttempt = true;
		if(this.registerForm.valid){
      var user: User = {
        id: "",
        username: this.registerForm.controls.username.value,
        password: this.registerForm.controls.password.value
      };
      var result=this.apiService.register(user).subscribe(data => console.log(data),
      error => console.log(error));
			/*.then(dataSaved =>{
				swal({
					title: 'Ohray! User registered!',
					icon: "info",
					timer: 2000,
					buttons: [false],
				}).then((value) => {
					this.navCtrl.popToRoot();
				});
			});
		}else{
			this.validateAllFormFields(this.addForm);
    }*/
    }
    
  }

  validateAllFormFields(formGroup: FormGroup) {     
		Object.keys(formGroup.controls).forEach(field => {  
			const control = formGroup.get(field);                         
			control.markAsDirty({ onlySelf: true });
		});
	}


  ngOnInit() {
  }

}
