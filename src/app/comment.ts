export class Comment {
    id: string;
    author: string;
    content: string;
    createdAt: string;
    updateAt: string;
}
