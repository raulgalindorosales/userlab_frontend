export class Article {
    id: string;
    author: string;
    title: string;
    content: string;
    createdAt: string;
    updateAt: string;
    comments: Comment[] = [];
}
