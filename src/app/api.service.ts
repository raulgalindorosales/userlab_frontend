import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../environments/environment';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  
  private apiUrl: String = environment.apiUrl;
  
  constructor(private httpClient: HttpClient) { }
  
  public register(newUser:User){
    return this.httpClient.post(`${this.apiUrl}/register`, newUser);
  }
}
